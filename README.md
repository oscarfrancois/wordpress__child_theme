# Les thèmes wordpress enfant ("child theme")

Cette activité va vous faire découvrir la notion de thème enfant.
C'est aussi un premier pas vers la création de votre propre thème wordpress.

## Pré-requis

[Configuration d'un conteneur ("container")](https://gitlab.com/oscarfrancois/cloud__container)

## Etapes

 - Choisissez un thème sur: https://wordpress.org/themes/browse/featured .
 - Choisissez au moins trois modifications à apporter à votre thème.
   2 modifications vous sont imposées: le changement de la couleur d'un icône du
   thème parent et l'affichage d'un logo vectoriel personnalisé au bas de chaque
   page du thème. La troisième modification est libre de choix.
 - Créez un thème enfant ("child theme") et implémentez ces modifications.

## Quelques généralités sur l'outil Wordpress

Présentation par l'enseignant.

## Installation d'un wordpress sur un conteneur

### Etapes

 - Création d'un conteneur sur le serveur proxmox de l'école (cf. pré-requis)
 - Activation du service web et de la base de données.
 - Installation de wordpress.
 - Configuation initiale de wordpress.

### Installation de wordpress

Directement depuis le conteneur:

`sudo apt install -y curl`

`cd /var/www`

`curl -L https://wordpress.org/latest.tar.gz -O`

`tar -xf latest.tar.gz`

## Activation de wordpress dans votre serveur web

Dans `/etc/nginx/sites-available/default` changez la directive:

`root /var/www/html;`

pour pointer vers le futur répertoire ou sera wordpress: `/var/www/wordpress;`

Rechargez ensuite la configuration de nginx via:

`sudo systemctl reload nginx.service`


### Configuation wordpress

Créez une base de données pour votre instance de wordpress:

`sudo mysql -u root -p -e 'create database MA_BASE_DE_DONNEES'`

`sudo mysql -u root -p -e 'show databases'`

Créez un utilisateur ayant tous les droits sur les bases de données:

`sudo mysql -uroot -p -e "grant all privileges on *.* to 'MON_UTILISATEUR'
identified by 'MON_MOT_DE_PASSE' with grant option;"`

Remarque: l'utilisation de phpmyadmin est une autre possibilité.

Pointez votre navigateur sur l'adresse de votre site wordpress puis suivez
l'assistant de configuration ("wizard").

## Choix d'un thème wordpress dans la partie administration wordpress

Connectez-vous à la partie administration de votre site:

`http://MON_SITE/wp-admin`

Puis choisissez et activez un thème via le menu: "Apparence -> Thème".

## La création d'un thème enfant

Ce chapitre décrit les notions essentielles pour réaliser un thème enfant.
Il n'a pas vocation à être exhaustif. En complément, de très nombreuses
ressources sur internet traitent de ce sujet.

### Pourquoi créer des thèmes enfants?

Les thèmes wordpress sont mis à jour régulièrement pour améliorer le rendu
graphique, rajouter des fonctionnalités, corriger des anomalies, pour rester
compatibles avec les nouvelles version de wordpress, etc.

Séparer le thème de base de vos personnalisations permet de garder votre site
maintenable dans le temps.

Figer un site à une version spécifique de Wordpress n'est pas une solution
viable professionnellement ne serait-ce que du fait des correctifs de sécurité
wordpress ou du thème qui peuvent être diffusés à tout moment.

Allez dans la partie administateur de votre site dans les menus:
  Apparence -> Editeur de thème
et constatez les recommandations correspondantes.

### Localisation d'un thème wordpress

Installez un nouveau thème de votre choix via les thèmes additionels de
wordpress.

Il n'est pas autorisé de laisser le thème par défaut (twentyfifteenchild).

### Etapes détaillées de la création d'un thème enfant

Veuillez prendre connaissance de l'aide en ligne wordpress pour le création d'un
thème enfant et mettez le en pratique sur votre propre site:

https://developer.wordpress.org/themes/advanced-topics/child-themes

Respectez les bonnes pratiques indiquées dans ce tutoriel (règles de nommage,
etc).

# Questions

- Quelles limites peut t'il tout de même y avoir lorsqu'on utilise un thème
enfant?

- Dans quel sous-répertoire de Wordpress se trouve installé le nouveau thème?

- Quelles sont les balises à ajouter et/ou à personnaliser dans la feuille de
style lorsqu'il s'agit d'un thème enfant?


- Dans la section:

`https://developer.wordpress.org/themes/advanced-topics/child-themes/#3-enqueue-stylesheet`

A quoi sert la ligne 10:

`array( $parent_style ),` ?

- Donnez un exemple de problème qui peut se produire si cette ligne n'est pas
incluse?

- Que se passe t'il si un fichier dans le répertoire du thème enfant existe déjà
dans le thème parent?

- Quelle est la particularité du fichier `functions.php` d'un thème enfant par
rapport au même fichier du thème parent? Est-ce que l'on procèdera de la même
façon qu'un autre fichier à adapter?

- Dans quels cas développer un plug-in wordpress plutôt que de créer un thème
enfant?

